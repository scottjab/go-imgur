
# go-imgur

a Go wrapper for the imgur.com api - [godoc here](http://godoc.org/bitbucket.org/liamstask/go-imgur/imgur).

[![Build Status](https://drone.io/bitbucket.org/liamstask/go-imgur/status.png)](https://drone.io/bitbucket.org/liamstask/go-imgur/latest)

There are *lots* of endpoints to support, I'm just adding a few as I need them. Please feel free to contribute others.

Most design liberally lifted from the [go-github](https://github.com/google/go-github) project.

# Contributors

Thank you!

* Andy Walker (walkeraj)
